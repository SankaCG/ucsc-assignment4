#include <stdio.h>

int main(void){

	float cel,far;

	printf("Enter temperature in celsius:");
	scanf("%f",&cel);

	far = ((float)cel*9/5)+32;

	printf("Temperature in Fahrenheit:%.2f\n",far);

	return 0;
}
