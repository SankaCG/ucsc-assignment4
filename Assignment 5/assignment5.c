#include <stdio.h>

int calcAttendence(int price);
int calcProfit(int price, int attendence);

int main(){

	int price;
	printf("Please enter a ticket price:");
	scanf("%d",&price);
	
	int at=calcAttendence(price);
	if (at == 0)
		printf("There will be no attendance.");
	else{
		printf("Total Number of Attendees:%d",at);
		printf("\nTotal Profit:%d",calcProfit(price,at));
	}

}

int calcAttendence(int price){	//used to calculate the number of attendees

	if(price<=45)
		return 120+((15-price)*4);
	else
		return 0;
}


int calcProfit(int price, int attendence){	//used to calculate the total profit

	return (price*attendence)-(500+(attendence*3));
}
